// Some parts of this code are unshamely copied from
// Mozilla's Bitch to Boss extension: https://addons.mozilla.org/fr/firefox/addon/b-itch-to-boss/
// and are so under MPL 2.0

// throttling global variables
let replaceWait = false;
let replaceWaitTime = 250; // quarter of a second
let replaceQueue = [];

// List of strings to proutify
const prouts = {
  "49.3": "49.prout",
  "\\bministre": "miniprout", // Does not proutify other words than "ministre"
  "agnès pannier-runacher": "agnès proutier-runacher",
  "amélie de montchalin": "amélie de proutalin",
  "aurore berger": "aurore berprout",
  "barbara pompili": "barbara proutili",
  "bashar al-assad":"bashar al-asprout",
  "bernard arnault": "petnard arnault",
  "bettencourt": "petencourt",
  "bfm": "petfm",
  "bill gates": "bill pète",
  "boris": "beauprout",
  "bruno le maire": "bruno le prout",
  "bruno lemaire": "bruno le prout",
  "bruno retailleau": "bruno retailleprout",
  "castaner": "cacastaner",
  "catherine colonna": "catherine colon",
  "ce que l'on sait": "ce que l'on proute",
  "cedric o": "cédric prout",
  "cédric o": "cédric prout",
  "christian estrosi": "christian estroprout",
  "christine bouton": "christine proutin",
  "christophe barbier": "christophe barpet",
  "christophe béchu": "christophe petchu",
  "confinement":"confiprout",
  "coronavirus":"coronaproutus",
  "couac": "prout",
  "covid": "coprout",
  "cyril hanouna": "cyril hanounaprout",
  "d'olivier véran": "de proutivier véran",
  "darmanin": "darmaprout",
  "dassault": "petault",
  "david rachline": "david proutline",
  "député": "déprouté",
  "edwige diaz": "edwige prout",
  "elisabeth borne": "élisabeth prout",
  "élisabeth borne": "élisabeth prout",
  "elon musk": "pet-lon musk",
  "emmanuelle wargon": "emmanuelle prouton",
  "erdogan":"erdoprout",
  "erdoğan":"erdoprout",
  "éric ciotti": "éric proutti",
  "éric dupond-moretti": "éric duprout-moretti",
  "florence parly": "florence proutly",
  "florian philippot": "floriant philiprout",
  "franck riester": "franck prouster",
  "frederique vidal": "frédérique proutal",
  "frédérique vidal": "frédérique proutal",
  "gabriel attal": "gabriel prouttal",
  "gazprom":"gazprout",
  "geneviève darrieussecq": "geneviève duproutsecq",
  "geoffroy roux de bézieux": "geoffroy prout de bézieux",
  "gérard larcher": "gérard larchiasse",
  "grégoire de fournas": "grégoire de fourprout",
  "hélène laporte": "hélène laprout",
  "j.k. rowling": "j.k. rowprout",
  "jair bolsonaro": "jair bolsonaprout",
  "jean castex": "jean proutex",
  "jean yves le drian": "jean-prout le drian",
  "jean-baptiste djebbari": "jean-baptiste djeprouti",
  "jean-michel blanquer": "jean-michel blanc-prout",
  "jeff bezos": "jeff petzos",
  "johnny deep": "johnny pet",
  "jordan bardella": "jordan proutella",
  "julien sanchez": "julien sanprout",
  "la manif pour tous": "la manif prout tous",
  "le pen": "le pen",
  "le figaro": "le figarot",
  "léa salamé": "léa salapet",
  "les républicains": "les petplubicains",
  "louis aliot": "louis proutiot",
  "macron": "maprout",
  "marc fesneau": "marc faitprout",
  "marlène schiappa": "marlène schiaprout",
  "mélenchon": "mélenprout",
  "nicolas sarkozy": "nicoprout sarkozy",
  "olivia grégroire": "olivia petgoire",
  "olivier dussopt": "olivier duprout",
  "olivier véran": "olivier pétant",
  "pap ndiaye": "prout ndiaye",
  "pascal praud": "pascal prout",
  "patrick drahi": "proutick drahi",
  "patrick pouyanné": "patrick proutanné",
  "plan de relance": "prout de relance",
  "poutine":"proutine",
  "président": "proutident",
  "protocole sanitaire": "proutocole sanitaire",
  "matignon": "proutignon",
  "rassemblement national": "prout-sent-vraiment national",
  "roselyne bachelot": "proutelyne bachelot",
  "sébastien chenu": "sébastien cheprout",
  "sébastion lecornu": "sébastien leproutu",
  "sénateur": "sénaprout",
  "sergueï lavrov": "sergueï lavprout",
  "stanislas guerini": "stanislas proutini",
  "sylvie retaileau": "sylvie retailleprout",
  "vincent bolloré": "vincent bolloprout",
  "xavier niel": "xavier prout",
  "yaël braun-pivet": "yaël prout-pivet",
  "zemmour": "zeprout",
};

// Join prouts in a regex
let regexString = "";
for (let key in prouts) {
  regexString += key + '|';
}
regexString = regexString.slice(0, -1); // Remove trailing pipe

const regex = new RegExp(regexString, "gi");

// Use case insensitive replacer
const replacer = (match) => {
  const delimiter = /([\s-]+)/;
  const index = match.toLowerCase() !== "ministre" ? match.toLowerCase() : "\\bministre";
  let needleWords = match.split(delimiter);
  var haystackWords = prouts[index].split(delimiter);

  // Capitalize and transform to UpperCase when needed
  for (let i = 0; i < needleWords.length; i++) {
    if (needleWords[i] === needleWords[i].toUpperCase()) { // whole word in UpperCase
      haystackWords[i] = haystackWords[i].toUpperCase();
    } else if (needleWords[i][0] === needleWords[i][0].toUpperCase()) { // Capitalize
      haystackWords[i] = haystackWords[i][0].toUpperCase() + haystackWords[i].slice(1);
    }
  }
  
  return haystackWords.join("");
};

function processQueue() {
  // clone queue
  let queue = replaceQueue.slice(0);
  // empty queue
  replaceQueue = [];
  // loop through clone
  queue.forEach( (mutations) => {
      replaceNodes(mutations);
  });
}

function setWait() {
  replaceWait = true;
  setTimeout(function () {
      replaceWait = false;
      timerCallback();
  }, replaceWaitTime);
}

function timerCallback() {
  if(replaceQueue.length > 0) {
      // if there are queued items, process them
      processQueue();
      // then set wait to do next batch
      setWait();
  } else {
      // if the queue has been empty for a full timer cycle
      // remove the wait time to process the next action
      replaceWait = false;
  }
}

// The callback used for the document body and title observers
function observerCallback(mutations) {
    // add to queue
    replaceQueue.push(mutations);
    if(!replaceWait) {
        processQueue();
        setWait();
    } // else the queue will be processed when the timer finishes
}

const replaceText = (v) => {
  v = v.replace(regex, replacer)
  return v
}
const handleText = (textNode) => {
    textNode.nodeValue = replaceText(textNode.nodeValue);
}

// Returns true if a node should *not* be altered in any way
const forbiddenTagNames = ['textarea', 'input', 'script', 'noscript', 'template', 'style'];
function isForbiddenNode(node) {
    if (node.isContentEditable) {
        return true;
    } else if (node.parentNode && node.parentNode.isContentEditable) {
        return true;
    } else {
        return forbiddenTagNames.includes(node.tagName?.toLowerCase());
    }
}

// The callback used for the document body and head observers
const replaceNodes = (mutations) => {
  let i, node;

  mutations.forEach(function(mutation) {
      for (i = 0; i < mutation.addedNodes.length; i++) {
          node = mutation.addedNodes[i];
          if (isForbiddenNode(node)) {
              // Should never operate on user-editable content
              continue;
          } else if (node.nodeType === 3) {
              // Replace the text for text nodes
              handleText(node);
          } else {
              // Otherwise, find text nodes within the given node and replace text
              walk(node);
          }
      }
  });
}

const walk = (rootNode) => {
  // Find all the text nodes in rootNode
  let walker = document.createTreeWalker(
      rootNode,
      NodeFilter.SHOW_TEXT,
      {
          acceptNode: function(node) {
              return /^(STYLE|SCRIPT)$/.test(node.parentElement.tagName) || /^\s*$/.test(node.data) ? NodeFilter.FILTER_REJECT : NodeFilter.FILTER_ACCEPT;
          }
      },
      false
  ),
  node;

  // Modify each text node's value
  while (node = walker.nextNode()) {
      handleText(node);
  }
}

// Walk the doc (document) body, replace the title, and observe the body and head
const walkAndObserve = (doc) => {
  let docHead = doc.getElementsByTagName('head')[0]
  let observerConfig = {
      characterData: true,
      childList: true,
      subtree: true
  }
  let bodyObserver
  let headObserver

  // Do the initial text replacements in the document body and title
  walk(doc.body);
  doc.title = replaceText(doc.title);

  // Observe the body so that we replace text in any added/modified nodes
  bodyObserver = new MutationObserver(observerCallback);
  bodyObserver.observe(doc.body, observerConfig);

  // Observe the title so we can handle any modifications there
  if (docHead) {
      headObserver = new MutationObserver(observerCallback);
      headObserver.observe(docHead, observerConfig);
  }
}

walkAndObserve(document)
